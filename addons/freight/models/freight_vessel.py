# -*- coding: utf-8 -*-
# Part of AYIRA MARITIME. See LICENSE file for full copyright and licensing details.

from dateutil.relativedelta import relativedelta

from odoo import api, fields, models, _


class FreightVessel(models.Model):
    _inherit = 'mail.thread'
    _name = 'freight.vessel'
    _description = 'Information on a vessel'
    _order = 'imo_no asc'

    def _get_default_state(self):
        state = self.env.ref('freight.vessel_state_active', raise_if_not_found=False)
        return state and state.id or False

    name = fields.Char(compute="_compute_vessel_name", store=True)
    active = fields.Boolean('Active', default=True, track_visibility="onchange")
    company_id = fields.Many2one('res.company', 'Company')
    imo_no = fields.Char('IMO Number', help='Unique number written on the vessel motor (IMO number)', copy=False)
    manager_id = fields.Many2one('res.partner', 'Manager', track_visibility="onchange", help='Manager of the vessel', copy=False)
    model_id = fields.Many2one('freight.vessel.model', 'Model', required=True, help='Model of the vessel')
    log_fuel = fields.One2many('freight.vessel.log.fuel', 'vessel_id', 'Fuel Logs')
    log_services = fields.One2many('freight.vessel.log.services', 'vessel_id', 'Services Logs')
    log_certificates = fields.One2many('freight.vessel.log.certificate', 'vessel_id', 'Certificates')
    cost_count = fields.Integer(compute="_compute_count_all", string="Costs")
    certificate_count = fields.Integer(compute="_compute_count_all", string='Certificates')
    service_count = fields.Integer(compute="_compute_count_all", string='Services')
    fuel_logs_count = fields.Integer(compute="_compute_count_all", string='Fuel Logs')
    speed_count = fields.Integer(compute="_compute_count_all", string='Speed')
    initial_registration_date = fields.Date('Initial Registration Date', required=False, help='Date when the vessel was inially registered')
    color = fields.Char(help='Color of the vessel')
    state_id = fields.Many2one('freight.vessel.state', 'State', default=_get_default_state, 
        help='Current state of the vessel', ondelete="set null")
    location = fields.Char(help='Location of the vessel (garage, ...)')
    year_built = fields.Char('Year Built',help='Year of the model')
    official_no = fields.Integer('Official Number', help='Number of official_no of the vessel', default=5)
    tag_ids = fields.Many2many('freight.vessel.tag', 'freight_vessel_vessel_tag_rel', 'vessel_tag_id', 'tag_id', 'Tags', copy=False)
    speed = fields.Float(compute='_get_speed', inverse='_set_speed', string='Last Speed',
        help='Speed measure of the vessel at the moment of this log')
    speed_unit = fields.Selection([
        ('knot', 'Knot'),
        ('nm', 'Nautical Mile')
        ], 'Speed Unit', default='knot', help='Unit of the speed ', required=True)
    transmission = fields.Selection([('manual', 'Manual'), ('automatic', 'Automatic')], 'Transmission', help='Transmission Used by the vessel')
    fuel_type = fields.Selection([
        ('gasoline', 'Gasoline'),
        ('diesel', 'Diesel'),
        ('electric', 'Electric'),
        ('hybrid', 'Hybrid')
        ], 'Fuel Type', help='Fuel Used by the vessel')
    horsepower = fields.Integer()
    horsepower_tax = fields.Float('Horsepower Taxation')
    power = fields.Integer('Power', help='Power in kW of the vessel')
    co2 = fields.Float('CO2 Emissions', help='CO2 emissions of the vessel')
    image = fields.Binary(related='model_id.image', string="Logo")
    image_medium = fields.Binary(related='model_id.image_medium', string="Logo (medium)")
    image_small = fields.Binary(related='model_id.image_small', string="Logo (small)")
    certificate_renewal_due_soon = fields.Boolean(compute='_compute_certificate_reminder', search='_search_certificate_renewal_due_soon',
        string='Has Certificates to renew', multi='certificate_info')
    certificate_renewal_overdue = fields.Boolean(compute='_compute_certificate_reminder', search='_search_get_overdue_certificate_reminder',
        string='Has Certificates Overdue', multi='certificate_info')
    certificate_renewal_name = fields.Text(compute='_compute_certificate_reminder', string='Name of certificate to renew soon', multi='certificate_info')
    certificate_renewal_total = fields.Text(compute='_compute_certificate_reminder', string='Total of certificates due or overdue minus one',
        multi='certificate_info')
    ship_value = fields.Float(string="Catalog Value (VAT Incl.)", help='Value of the bought vessel')
    residual_value = fields.Float()

    _sql_constraints = [
        ('manager_id_unique', 'UNIQUE(manager_id)', 'Only one ship can be assigned to the same employee!')
    ]

    @api.depends('model_id.brand_id.name', 'model_id.name')
    def _compute_vessel_name(self):
        for record in self:
            record.name = record.model_id.brand_id.name + '/' + record.model_id.name + '/'

    def _get_speed(self):
        FreightVehicalSpeed = self.env['freight.vessel.speed']
        for record in self:
            vessel_speed = FreightVehicalSpeed.search([('vessel_id', '=', record.id)], limit=1, order='value desc')
            if vessel_speed:
                record.speed = vessel_speed.value
            else:
                record.speed = 0

    def _set_speed(self):
        for record in self:
            if record.speed:
                date = fields.Date.context_today(record)
                data = {'value': record.speed, 'date': date, 'vessel_id': record.id}
                self.env['freight.vessel.speed'].create(data)

    def _compute_count_all(self):
        Speed = self.env['freight.vessel.speed']
        LogFuel = self.env['freight.vessel.log.fuel']
        LogService = self.env['freight.vessel.log.services']
        LogCertificate = self.env['freight.vessel.log.certificate']
        Cost = self.env['freight.vessel.cost']
        for record in self:
            record.speed_count = Speed.search_count([('vessel_id', '=', record.id)])
            record.fuel_logs_count = LogFuel.search_count([('vessel_id', '=', record.id)])
            record.service_count = LogService.search_count([('vessel_id', '=', record.id)])
            record.certificate_count = LogCertificate.search_count([('vessel_id', '=', record.id),('state','!=','closed')])
            record.cost_count = Cost.search_count([('vessel_id', '=', record.id), ('parent_id', '=', False)])

    @api.depends('log_certificates')
    def _compute_certificate_reminder(self):
        for record in self:
            overdue = False
            due_soon = False
            total = 0
            name = ''
            for element in record.log_certificates:
                if element.state in ('open', 'expired') and element.expiration_date:
                    current_date_str = fields.Date.context_today(record)
                    due_time_str = element.expiration_date
                    current_date = fields.Date.from_string(current_date_str)
                    due_time = fields.Date.from_string(due_time_str)
                    diff_time = (due_time - current_date).days
                    if diff_time < 0:
                        overdue = True
                        total += 1
                    if diff_time < 15 and diff_time >= 0:
                            due_soon = True
                            total += 1
                    if overdue or due_soon:
                        log_certificate = self.env['freight.vessel.log.certificate'].search([
                            ('vessel_id', '=', record.id),
                            ('state', 'in', ('open', 'expired'))
                            ], limit=1, order='expiration_date asc')
                        if log_certificate:
                            # we display only the name of the oldest overdue/due soon certificate
                            name = log_certificate.cost_subtype_id.name

            record.certificate_renewal_overdue = overdue
            record.certificate_renewal_due_soon = due_soon
            record.certificate_renewal_total = total - 1  # we remove 1 from the real total for display purposes
            record.certificate_renewal_name = name

    def _search_certificate_renewal_due_soon(self, operator, value):
        res = []
        assert operator in ('=', '!=', '<>') and value in (True, False), 'Operation not supported'
        if (operator == '=' and value is True) or (operator in ('<>', '!=') and value is False):
            search_operator = 'in'
        else:
            search_operator = 'not in'
        today = fields.Date.context_today(self)
        datetime_today = fields.Datetime.from_string(today)
        limit_date = fields.Datetime.to_string(datetime_today + relativedelta(days=+15))
        self.env.cr.execute("""SELECT cost.vessel_id,
                        count(certificate.id) AS certificate_number
                        FROM freight_vessel_cost cost
                        LEFT JOIN freight_vessel_log_certificate certificate ON certificate.cost_id = cost.id
                        WHERE certificate.expiration_date IS NOT NULL
                          AND certificate.expiration_date > %s
                          AND certificate.expiration_date < %s
                          AND certificate.state IN ('open', 'expired')
                        GROUP BY cost.vessel_id""", (today, limit_date))
        res_ids = [x[0] for x in self.env.cr.fetchall()]
        res.append(('id', search_operator, res_ids))
        return res

    def _search_get_overdue_certificate_reminder(self, operator, value):
        res = []
        assert operator in ('=', '!=', '<>') and value in (True, False), 'Operation not supported'
        if (operator == '=' and value is True) or (operator in ('<>', '!=') and value is False):
            search_operator = 'in'
        else:
            search_operator = 'not in'
        today = fields.Date.context_today(self)
        self.env.cr.execute('''SELECT cost.vessel_id,
                        count(certificate.id) AS certificate_number
                        FROM freight_vessel_cost cost
                        LEFT JOIN freight_vessel_log_certificate certificate ON certificate.cost_id = cost.id
                        WHERE certificate.expiration_date IS NOT NULL
                          AND certificate.expiration_date < %s
                          AND certificate.state IN ('open', 'expired')
                        GROUP BY cost.vessel_id ''', (today,))
        res_ids = [x[0] for x in self.env.cr.fetchall()]
        res.append(('id', search_operator, res_ids))
        return res

    @api.onchange('model_id')
    def _onchange_model(self):
        if self.model_id:
            self.image_medium = self.model_id.image
        else:
            self.image_medium = False

    @api.model
    def create(self, data):
        vessel = super(FreightVessel, self.with_context(mail_create_nolog=True)).create(data)
        vessel.message_post(body=_('%s has been added to the freight!') % (vessel.model_id.name))
        return vessel

    @api.multi
    def write(self, vals):
        """
        This function write an entry in the openchatter whenever we change important information
        on the vessel like the model, the drive, the state of the vessel or its license plate
        """
        for vessel in self:
            changes = []
            if 'model_id' in vals and vessel.model_id.id != vals['model_id']:
                value = self.env['freight.vessel.model'].browse(vals['model_id']).name
                oldmodel = vessel.model_id.name or _('None')
                changes.append(_("Model: from '%s' to '%s'") % (oldmodel, value))
            if 'manager_id' in vals and vessel.manager_id.id != vals['manager_id']:
                value = self.env['res.partner'].browse(vals['manager_id']).name
                oldmanager = (vessel.manager_id.name) or _('None')
                changes.append(_("Manager: from '%s' to '%s'") % (oldmanager, value))
            if 'state_id' in vals and vessel.state_id.id != vals['state_id']:
                value = self.env['freight.vessel.state'].browse(vals['state_id']).name
                oldstate = vessel.state_id.name or _('None')
                changes.append(_("State: from '%s' to '%s'") % (oldstate, value))

            if len(changes) > 0:
                self.message_post(body=", ".join(changes))

            return super(FreightVessel, self).write(vals)

    @api.multi
    def return_action_to_open(self):
        """ This opens the xml view specified in xml_id for the current vessel """
        self.ensure_one()
        xml_id = self.env.context.get('xml_id')
        if xml_id:
            res = self.env['ir.actions.act_window'].for_xml_id('freight', xml_id)
            res.update(
                context=dict(self.env.context, default_vessel_id=self.id, group_by=False),
                domain=[('vessel_id', '=', self.id)]
            )
            return res
        return False

    @api.multi
    def act_show_log_cost(self):
        """ This opens log view to view and add new log for this vessel, groupby default to only show effective costs
            @return: the costs log view
        """
        self.ensure_one()
        copy_context = dict(self.env.context)
        copy_context.pop('group_by', None)
        res = self.env['ir.actions.act_window'].for_xml_id('freight', 'freight_vessel_costs_action')
        res.update(
            context=dict(copy_context, default_vessel_id=self.id, search_default_parent_false=True),
            domain=[('vessel_id', '=', self.id)]
        )
        return res


class FreightVesselSpeed(models.Model):
    _name = 'freight.vessel.speed'
    _description = 'Speed log for a vessel'
    _order = 'date desc'

    name = fields.Char(compute='_compute_vessel_log_name', store=True)
    date = fields.Date(default=fields.Date.context_today)
    value = fields.Float('Speed Value', group_operator="max")
    vessel_id = fields.Many2one('freight.vessel', 'Vessel', required=True)
    unit = fields.Selection(related='vessel_id.speed_unit', string="Unit", readonly=True)
    manager_id = fields.Many2one(related="vessel_id.manager_id", string="Manager")

    @api.depends('vessel_id', 'date')
    def _compute_vessel_log_name(self):
        for record in self:
            name = record.vessel_id.name
            if not name:
                name = record.date
            elif record.date:
                name += ' / ' + record.date
            record.name = name

    @api.onchange('vessel_id')
    def _onchange_vessel(self):
        if self.vessel_id:
            self.unit = self.vessel_id.speed_unit


class FreightVesselState(models.Model):
    _name = 'freight.vessel.state'
    _order = 'sequence asc'

    name = fields.Char(required=True)
    sequence = fields.Integer(help="Used to order the note stages")

    _sql_constraints = [('freight_state_name_unique', 'unique(name)', 'State name already exists')]


class FreightVesselTag(models.Model):
    _name = 'freight.vessel.tag'

    name = fields.Char(required=True, translate=True)
    color = fields.Integer('Color Index', default=10)

    _sql_constraints = [('name_uniq', 'unique (name)', "Tag name already exists !")]


class FreightServiceType(models.Model):
    _name = 'freight.service.type'
    _description = 'Type of services available on a vessel'

    name = fields.Char(required=True, translate=True)
    category = fields.Selection([
        ('certificate', 'Certificate'),
        ('service', 'Service')
        ], 'Category', required=True, help='Choose whether the service refer to certificates, vessel services or both')
