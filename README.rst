======================
odoo vessel management
======================




.. image:: https://pyup.io/repos/github/ayira/odoo_vessel_management/shield.svg
     :target: https://pyup.io/repos/github/ayira/odoo_vessel_management/
     :alt: Updates



This is an module that enables odoo manage all of your vessls, the certificates, certificates, services and cost associated with the management of your fleet of vessels.



Features
--------

* TODO

Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
